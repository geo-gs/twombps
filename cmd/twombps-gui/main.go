/*
Twombps-GUI launches a GUI job to encode a video.
*/
package main

import (
	"gitlab.com/geo-gs/twombps/daemon"
	"gitlab.com/geo-gs/twombps/log"
	"gitlab.com/geo-gs/twombps/params"
)

func main() {
	params := params.FromPrompts()
	title := "Encoding " + params.OutputBase()
	err := log.LaunchProgressGui(title)
	if err != nil {
		log.Println(err)
	}
	daemon.StartDaemon(params)
	err = log.CloseGui()
	if err != nil {
		log.Println(err)
	}
}
