/*
Twombps-CLI launches a job to encode a video from the CLI.
*/
package main

import (
	"gitlab.com/geo-gs/twombps/daemon"
	"gitlab.com/geo-gs/twombps/params"
)

func main() {
	params := params.ParseFlags()
	daemon.StartDaemon(params)
}
