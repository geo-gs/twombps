/*
Package daemon contains the damon logic shared by the GUI and CLI binaries.
*/
package daemon

import (
	"os"
	"path"
	"time"

	"gitlab.com/geo-gs/twombps/log"
	"gitlab.com/geo-gs/twombps/modules"
	"gitlab.com/geo-gs/twombps/params"
	"gitlab.com/geo-gs/twombps/types"
	"gitlab.com/geo-gs/twombps/utils"
)

// StartDaemon starts the daemon job.
func StartDaemon(params *params.Parameters) {
	params.Println()
	if !params.Valid() {
		return
	}
	inputs := params.Inputs()
	var input string
	if len(inputs) > 1 {
		input = modules.Concat(inputs, path.Base(input))
	} else if len(inputs) == 0 {
		log.Println("No input supplied.")
		return
	} else {
		input = inputs[0]
	}
	if input == "" {
		return
	}
	movie := types.GetMedia(input)
	if movie == nil {
		log.Println("input is not supported")
		return
	}
	start := time.Now()
	log.Printf("### Optimizing %s.\n", movie.Name())
	audioPath, err := modules.AudioBackup(movie)
	if err != nil {
		log.Printf("### audio backup failed: %v\n", err)
		return
	}
	optimizedVideoPath, err := modules.Optimize(movie.Name(), movie.Video().Path(), audioPath)
	if err != nil {
		log.Printf("### video failed to optimize: %v\n", err)
		return
	}
	output := params.Output()
	if output == "" {
		output = input
	}
	if utils.PathExists(output) {
		err = os.Remove(output)
		if err != nil {
			log.Printf("### output already exists and could not be removed: %v\n", err)
		}
	}
	err = os.Rename(optimizedVideoPath, output)
	if err != nil {
		log.Printf("### optimized video could not be moved: %v\n", err)
		return
	}
	err = os.RemoveAll(utils.WorkingDirPath(movie.Name()))
	if err != nil {
		log.Printf("### working directory could not be removed: %v\n", err)
	}
	params.Cleanup(movie.Path())
	if len(inputs) > 1 {
		// clean up concat temp file
		err = os.Remove(input)
		if err != nil {
			log.Println(err)
		}
	}
	log.Println(movie.Name(), "done in", time.Since(start).Hours(), "hours")
}
