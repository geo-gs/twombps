/*
Package log contains the logic on where and how to write log
messages. Probably should just use golang's built in logging
class but I am more familiar with this.
*/
package log

import (
	"fmt"
	"strings"

	"github.com/ncruces/zenity"
)

var (
	progressGui zenity.ProgressDialog
	log         = []string{""}
)

// LaunchProgressGui will launch the progress GUI.
// Once launched logs will be written to the GUI.
func LaunchProgressGui(title string) (err error) {
	progressGui, err = zenity.Progress(zenity.Title(title), zenity.Pulsate())
	if err != nil {
		return err
	}
	err = progressGui.Text("...")
	return
}

// CloseGui will close the progress GUI.
func CloseGui() (err error) {
	err = progressGui.Complete()
	if err != nil {
		return
	}
	err = progressGui.Close()
	return
}

// Print formats using the default formats for its operands and writes to the log.
// Spaces are added between operands when neither is a string.
func Print(a ...any) {
	i := len(log) - 1
	log[i] = log[i] + fmt.Sprint(a...)
	exportToGui()
	fmt.Print(a...)
}

// Println formats using the default formats for its operands and writes to the log.
// Spaces are always added between operands and a newline is appended.
func Println(a ...any) {
	i := len(log) - 1
	log[i] = strings.TrimSuffix(log[i]+fmt.Sprintln(a...), "\n")
	log = append(log, "")
	exportToGui()
	fmt.Println(a...)
}

// Printf formats according to a format specifier and writes to the log.
// It returns the number of bytes written and any write error encountered.
func Printf(format string, a ...any) {
	line := fmt.Sprintf(format, a...)
	if strings.HasSuffix(line, "\n") {
		line = strings.TrimRight(line, "\n")
	}
	i := len(log) - 1
	log[i] = log[i] + line
	log = append(log, "")
	exportToGui()
	fmt.Printf(format, a...)
}

// exportToGui exports the last n amount of lines from the logs to the progress GUI.
func exportToGui() {
	if progressGui != nil {
		tail := ""
		for i, line := range log {
			if i > len(log)-10 {
				tail = tail + line + "\n"
			}
		}
		progressGui.Text(tail) // #nosec
	}
}
