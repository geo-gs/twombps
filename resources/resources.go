/*
Package resources containing the static resources.
*/
package resources

import (
	_ "embed" // blank import is a compile-time dependency
)

//go:embed resources/nnedi3_weights.bin
var nnedi3weights []byte

// Nnedi3Weights returns the nnedi3 weights
func Nnedi3Weights() []byte {
	return nnedi3weights
}
