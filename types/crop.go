/*
Package types containing the crop type.
*/
package types

import (
	"strconv"
	"strings"
)

// Crop is the crop struct
type Crop struct {
	filter string
}

// Filter returns the crop filter
func (c *Crop) Filter() string {
	return c.filter
}

// Width returns the crop width
func (c *Crop) Width() int {
	width, _ := strconv.Atoi(strings.Split(strings.Split(c.filter, "crop=")[1], ":")[0])
	return width
}

// Height returns the crop height
func (c *Crop) Height() int {
	height, _ := strconv.Atoi(strings.Split(strings.Split(c.filter, ":")[1], ":")[0])
	return height
}
