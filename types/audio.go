/*
Package types containing the audio type.
*/
package types

import (
	"os/exec"
	"strconv"
	"strings"

	"gitlab.com/geo-gs/twombps/utils"
)

// Audio is the audio container struct
type Audio struct {
	path     string
	channels *int
	bitrate  *int
	codec    string
}

// SetPath sets the audio's path
func (a *Audio) SetPath(path string) {
	a.path = path
}

// Channels returns the audio's number of channels
func (a *Audio) Channels() int {
	if a.channels != nil {
		return *a.channels
	}
	cmd := exec.Command(
		utils.FfprobePath(),
		"-v", "error",
		"-select_streams", "a:0",
		"-of", "default=noprint_wrappers=1:nokey=1",
		"-show_entries", "stream=channels",
		a.path) // #nosec
	stdout, _ := cmd.Output()
	channels, _ := strconv.Atoi(strings.TrimSuffix(string(stdout), "\n"))
	a.channels = &channels
	return *a.channels
}

// MaxBitrate returns the maximum audio bitrate to use
func (a *Audio) MaxBitrate() int {
	return 90000 + (a.Channels()-1)*12000
}

// Bitrate returns the audio's bitrate
func (a *Audio) Bitrate() int {
	if a.bitrate != nil {
		return *a.bitrate
	}
	cmd := exec.Command(
		utils.FfprobePath(),
		"-v", "error",
		"-select_streams", "a:0",
		"-of", "default=noprint_wrappers=1:nokey=1",
		"-show_entries", "stream=bit_rate",
		a.path) // #nosec
	stdout, _ := cmd.Output()
	bitrateStr := strings.TrimSuffix(string(stdout), "\n")
	if bitrateStr == "" || bitrateStr == "N/A" {
		bitrate := -1
		a.bitrate = &bitrate
		return *a.bitrate
	}
	bitrate, _ := strconv.Atoi(bitrateStr)
	a.bitrate = &bitrate
	return *a.bitrate
}

// IsEfficient returns true when the codec is efficient enough to not need compression.
func (a *Audio) IsEfficient() bool {
	switch " " + a.Codec() + " " {
	case " aac ", "aac_he", " aac_latm ", " mp3 ", " opus ", " vorbis ":
		return true
	default:
		return false
	}
}

// Codec returns the audio's codec
func (a *Audio) Codec() string {
	if a.codec != "" {
		return a.codec
	}
	cmd := exec.Command(
		utils.FfprobePath(),
		"-v", "error",
		"-select_streams", "a:0",
		"-of", "default=noprint_wrappers=1:nokey=1",
		"-show_entries", "stream=codec_name",
		a.path) // #nosec
	stdout, _ := cmd.Output()
	a.codec = strings.TrimSuffix(string(stdout), "\n")
	return a.codec
}
