/*
Package types containing the video type
*/
package types

import (
	"bytes"
	"fmt"
	"os/exec"
	"strconv"
	"strings"

	"gitlab.com/geo-gs/twombps/log"
	"gitlab.com/geo-gs/twombps/utils"
)

// Video is the video container struct
type Video struct {
	name              string
	path              string
	width             *int
	height            *int
	pixFmt            string
	bitrate           *int
	colorPrimaries    string
	dar               string
	sar               *float64
	crop              *Crop
	duration          string
	codec             string
	frameRateDividend *int
	frameRateDivisor  *int
	progressive       *bool
}

// Name returns the video's name
func (v *Video) Name() string {
	return v.name
}

// SetName sets the video's name
func (v *Video) SetName(name string) {
	v.name = name
}

// Path returns the video's path
func (v *Video) Path() string {
	return v.path
}

// SetPath sets the video's path
func (v *Video) SetPath(path string) {
	v.path = path
}

// Width returns the video's width
func (v *Video) Width() int {
	if v.width != nil {
		return *v.width
	}
	cmd := exec.Command(
		utils.FfprobePath(),
		"-v", "error",
		"-select_streams", "v:0",
		"-of", "default=noprint_wrappers=1:nokey=1",
		"-show_entries", "stream=width",
		v.path) // #nosec
	stdout, _ := cmd.Output()
	width, _ := strconv.Atoi(strings.TrimSuffix(string(stdout), "\n"))
	v.width = &width
	return *v.width
}

// Height returns the video's height
func (v *Video) Height() int {
	if v.height != nil {
		return *v.height
	}
	cmd := exec.Command(
		utils.FfprobePath(),
		"-v", "error",
		"-select_streams", "v:0",
		"-of", "default=noprint_wrappers=1:nokey=1",
		"-show_entries", "stream=height",
		v.path) // #nosec
	stdout, _ := cmd.Output()
	height, _ := strconv.Atoi(strings.TrimSuffix(string(stdout), "\n"))
	v.height = &height
	return *v.height
}

// PixFmt returns the video's pixel format
func (v *Video) PixFmt() string {
	if v.pixFmt != "" {
		return v.pixFmt
	}
	cmd := exec.Command(
		utils.FfprobePath(),
		"-v", "error",
		"-select_streams", "v:0",
		"-of", "default=noprint_wrappers=1:nokey=1",
		"-show_entries", "stream=pix_fmt",
		v.path) // #nosec
	stdout, _ := cmd.Output()
	v.pixFmt = strings.TrimSuffix(string(stdout), "\n")
	return v.pixFmt
}

// ColorPrimaries returns the video's color primaries
func (v *Video) ColorPrimaries() string {
	if v.colorPrimaries != "" {
		return v.colorPrimaries
	}
	cmd := exec.Command(
		utils.FfprobePath(),
		"-v", "error",
		"-select_streams", "v:0",
		"-of", "default=noprint_wrappers=1:nokey=1",
		"-show_entries", "stream=color_primaries",
		v.path) // #nosec
	stdout, _ := cmd.Output()
	v.colorPrimaries = strings.TrimSuffix(string(stdout), "\n")
	return v.colorPrimaries
}

// Bitrate returns the video's bitrate
func (v *Video) Bitrate() int {
	if v.bitrate != nil {
		return *v.bitrate
	}
	cmd := exec.Command(
		utils.FfprobePath(),
		"-v", "error",
		"-select_streams", "v:0",
		"-of", "default=noprint_wrappers=1:nokey=1",
		"-show_entries", "stream=bit_rate",
		v.path) // #nosec
	stdout, _ := cmd.Output()
	bitrateStr := strings.TrimSuffix(string(stdout), "\n")
	if bitrateStr == "" || bitrateStr == "N/A" {
		bitrate := -1
		v.bitrate = &bitrate
		return *v.bitrate
	}
	bitrate, _ := strconv.Atoi(bitrateStr)
	v.bitrate = &bitrate
	return *v.bitrate
}

// Dar returns the video's DAR
func (v *Video) Dar() string {
	if v.dar != "" {
		return v.dar
	}
	cmd := exec.Command(
		utils.FfprobePath(),
		"-v", "error",
		"-select_streams", "v:0",
		"-of", "default=noprint_wrappers=1:nokey=1",
		"-show_entries", "stream=display_aspect_ratio",
		v.path) // #nosec
	stdout, _ := cmd.Output()
	v.dar = strings.Replace(strings.TrimSuffix(string(stdout), "\n"), ":", "/", -1)
	return v.dar
}

// Dar64 returns the video's DAR as a float64.
func (v *Video) Dar64() float64 {
	dar := v.Dar()
	left, _ := strconv.ParseFloat(strings.Split(dar, "/")[0], 64)
	right, _ := strconv.ParseFloat(strings.Split(dar, "/")[1], 64)
	return left / right
}

// Sar returns the video's SAR
func (v *Video) Sar() float64 {
	if v.sar != nil {
		return *v.sar
	}
	cmd := exec.Command(
		utils.FfprobePath(),
		"-v", "error",
		"-select_streams", "v:0",
		"-of", "default=noprint_wrappers=1:nokey=1",
		"-show_entries", "stream=sample_aspect_ratio",
		v.path) // #nosec
	stdout, _ := cmd.Output()
	sarRatio := strings.TrimSuffix(string(stdout), "\n")
	left, _ := strconv.ParseFloat(strings.Split(sarRatio, ":")[0], 64)
	right, _ := strconv.ParseFloat(strings.Split(sarRatio, ":")[1], 64)
	sar := left / right
	v.sar = &sar
	return *v.sar
}

// Duration returns the video's duration
func (v *Video) Duration() string {
	if v.duration != "" {
		return v.duration
	}
	cmd := exec.Command(
		utils.FfprobePath(),
		"-v", "error",
		"-select_streams", "v:0",
		"-of", "default=noprint_wrappers=1:nokey=1",
		"-show_entries", "format=duration",
		v.path) // #nosec
	stdout, _ := cmd.Output()
	v.duration = strings.TrimSuffix(string(stdout), "\n")
	return v.duration
}

// Duration64 returns the video's duration as a float64.
func (v *Video) Duration64() float64 {
	seconds, _ := strconv.ParseFloat(v.Duration(), 64)
	return seconds
}

// DurationMs returns the video's duration in milliseconds.
func (v *Video) DurationMs() int {
	seconds, _ := strconv.ParseFloat(v.Duration(), 64)
	return int(seconds * 1000)
}

func calcFrameRateFromAvg(path string) (*int, *int) {
	ffprobeParams := []string{}
	ffprobeParams = append(ffprobeParams, "-v", "error")
	ffprobeParams = append(ffprobeParams, "-select_streams", "v:0")
	ffprobeParams = append(ffprobeParams, "-of", "default=noprint_wrappers=1:nokey=1")
	ffprobeParams = append(ffprobeParams, "-show_entries", "stream=avg_frame_rate")
	ffprobeParams = append(ffprobeParams, path)
	cmd := exec.Command(utils.FfprobePath(), ffprobeParams...) // #nosec
	stdout, _ := cmd.Output()
	frameRateRatio := strings.TrimSuffix(string(stdout), "\n")
	frameRateDividend, _ := strconv.Atoi(strings.Split(frameRateRatio, "/")[0])
	frameRateDivisor, _ := strconv.Atoi(strings.Split(frameRateRatio, "/")[1])
	frameRate := float64(frameRateDividend) / float64(frameRateDivisor)
	approxFrameRateDividend := 30000
	approxFrameRateDivisor := 1001
	if frameRate < float64(24.3) {
		approxFrameRateDividend = 24000
		approxFrameRateDivisor = 1001
	}
	if frameRate < float64(26) {
		approxFrameRateDividend = 25
		approxFrameRateDivisor = 1
	}
	if frameRate < float64(46) {
		approxFrameRateDividend = 30000
		approxFrameRateDivisor = 1001
	}
	if frameRate < float64(49) {
		approxFrameRateDividend = 24000
		approxFrameRateDivisor = 1001
	}
	if frameRate < float64(51) {
		approxFrameRateDividend = 25
		approxFrameRateDivisor = 1
	}
	return &approxFrameRateDividend, &approxFrameRateDivisor
}

// Codec returns the video's codec.
func (v *Video) Codec() string {
	if v.codec != "" {
		return v.codec
	}
	cmd := exec.Command(
		utils.FfprobePath(),
		"-v", "error",
		"-select_streams", "v:0",
		"-of", "default=noprint_wrappers=1:nokey=1",
		"-show_entries", "stream=codec_name",
		v.path) // #nosec
	stdout, _ := cmd.Output()
	v.codec = strings.TrimSuffix(string(stdout), "\n")
	return v.codec
}

// QueryFrameRate returns the video's query frame rate.
func (v *Video) QueryFrameRate() (*int, *int) {
	cmd := exec.Command(
		utils.FfprobePath(),
		"-v", "error",
		"-select_streams", "v:0",
		"-of", "default=noprint_wrappers=1:nokey=1",
		"-show_entries", "stream=r_frame_rate",
		v.path) // #nosec
	stdout, _ := cmd.Output()
	fpsRatio := strings.TrimSuffix(string(stdout), "\n")
	if fpsRatio == "" || strings.HasSuffix(fpsRatio, "/0") {
		return calcFrameRateFromAvg(v.path)
	}
	frameRateDividend, _ := strconv.Atoi(strings.Split(fpsRatio, "/")[0])
	frameRateDivisor, _ := strconv.Atoi(strings.Split(fpsRatio, "/")[1])
	return &frameRateDividend, &frameRateDivisor
}

// FrameRate returns the video's frames per second rate as a float64
func (v *Video) FrameRate() float64 {
	if v.frameRateDividend == nil || v.frameRateDivisor == nil {
		v.frameRateDividend, v.frameRateDivisor = v.QueryFrameRate()
	}
	return float64(*v.frameRateDividend) / float64(*v.frameRateDivisor)
}

// FrameRateStr returns the video's frames per second rate as a string ratio.
func (v *Video) FrameRateStr() string {
	if v.frameRateDividend == nil || v.frameRateDivisor == nil {
		v.frameRateDividend, v.frameRateDivisor = v.QueryFrameRate()
	}
	quotient := *v.frameRateDividend / *v.frameRateDivisor
	remainder := *v.frameRateDividend % *v.frameRateDivisor
	if remainder == 0 && (quotient == 24 || quotient == 48) {
		return "24/1"
	}
	if remainder == 0 && (quotient == 25 || quotient == 50) {
		return "25/1"
	}
	if remainder == 0 && (quotient == 30 || quotient == 60) {
		return "30/1"
	}
	frameRate := float64(*v.frameRateDividend) / float64(*v.frameRateDivisor)
	if frameRate < float64(24.3) {
		return "24000/1001"
	}
	if frameRate < float64(26) {
		return "25/1"
	}
	if frameRate < float64(46) {
		return "30000/1001"
	}
	if frameRate < float64(49) {
		return "24000/1001"
	}
	if frameRate < float64(51) {
		return "25/1"
	}
	if frameRate < float64(62) {
		return "30000/1001"
	}
	return fmt.Sprintf("%d/%d", *v.frameRateDividend, *v.frameRateDivisor)
}

// DetectCrop sets the vidwo's crop filter
func (v *Video) DetectCrop() {
	v.Crop()
}

// Crop returns the video's crop filter
func (v *Video) Crop() *Crop {
	if v.crop != nil {
		return v.crop
	}
	log.Println("Detecting video's crop...")
	ffmpegParams := []string{}
	ffmpegParams = append(ffmpegParams, "-t", "1000")
	ffmpegParams = append(ffmpegParams, "-i", v.path)
	ffmpegParams = append(ffmpegParams, "-vf", "select=not(mod(n\\,1000)),cropdetect=36:1:0")
	ffmpegParams = append(ffmpegParams, "-f", "null", "-")
	cmd := exec.Command(utils.FfmpegPath(), ffmpegParams...) // #nosec
	var b bytes.Buffer
	cmd.Stderr = &b //FFmpeg uses stderr for logging and progress information
	err := cmd.Run()
	if err != nil {
		log.Println("Unable to execute:", utils.FfmpegPath(), strings.Join(ffmpegParams, " "))
		v.crop.filter = fmt.Sprintf("crop=%v:%v:0:0", v.Width(), v.Height())
		return v.crop
	}
	logs := strings.Split(string(b.Bytes()), "\n")
	v.crop = &Crop{}
	for _, s := range logs {
		if strings.Contains(s, "crop=") {
			v.crop.filter = "crop=" + strings.Split(s, "crop=")[1]
		}
	}
	if v.crop.filter == "" {
		v.crop.filter = fmt.Sprintf("crop=%v:%v:0:0", v.Width(), v.Height())
	}
	log.Println("Crop detection results:", v.crop.filter)
	return v.crop
}

// Progressive returns true when the video is progressive
func (v *Video) Progressive() bool {
	if v.progressive != nil {
		return *v.progressive
	}
	log.Println("Detecting if video is progressive...")
	ffmpegParams := []string{}
	ffmpegParams = append(ffmpegParams, "-i", v.path)
	ffmpegParams = append(ffmpegParams, "-vf", "idet")
	ffmpegParams = append(ffmpegParams, "-f", "null", "-")
	cmd := exec.Command(utils.FfmpegPath(), ffmpegParams...) // #nosec
	var b bytes.Buffer
	cmd.Stderr = &b
	err := cmd.Run()
	if err != nil {
		log.Println("Unable to execute:", utils.FfmpegPath(), strings.Join(ffmpegParams, " "))
		return false
	}
	logs := strings.Split(string(b.Bytes()), "\n")
	idet := ""
	for _, s := range logs {
		if strings.Contains(s, "TFF:") && strings.Contains(s, "BFF:") && strings.Contains(s, "Progressive:") && strings.Contains(s, "Undetermined:") {
			idet = s
		}
	}
	log.Println("Progressive detection results:", idet)
	tff, _ := strconv.Atoi(strings.TrimSpace(strings.Split(strings.Split(idet, "TFF:")[1], "BFF:")[0]))
	bff, _ := strconv.Atoi(strings.TrimSpace(strings.Split(strings.Split(idet, "BFF:")[1], "Progressive:")[0]))
	pro, _ := strconv.Atoi(strings.TrimSpace(strings.Split(strings.Split(idet, "Progressive:")[1], "Undetermined:")[0]))
	progressive := pro > tff+bff
	v.progressive = &progressive
	return *v.progressive
}

// DetectScenes returns a list of timestamped scenes from the video
func (v *Video) DetectScenes() ([]string, error) {
	log.Println("Detecting Scenes...")
	ffprobeParams := []string{}
	ffprobeParams = append(ffprobeParams, "-show_frames")
	ffprobeParams = append(ffprobeParams, "-show_entries", "frame=best_effort_timestamp_time")
	ffprobeParams = append(ffprobeParams, "-of", "compact=p=0")
	ffprobeParams = append(ffprobeParams, "-f", "lavfi")
	ffprobeParams = append(ffprobeParams, "-i", fmt.Sprintf("movie='%s',select=gt(scene\\,0.5)", v.path))
	ffprobeParams = append(ffprobeParams, "-loglevel", "error")
	cmd := exec.Command(utils.FfprobePath(), ffprobeParams...) // #nosec
	var b bytes.Buffer
	cmd.Stdout = &b //FFmpeg uses stdout for outputing media
	err := cmd.Run()
	if err != nil {
		log.Println("Unable to execute:", utils.FfprobePath(), strings.Join(ffprobeParams, " "))
		return nil, err
	}
	scenes := strings.Split(string(b.Bytes()), "\n")
	return scenes, nil
}
