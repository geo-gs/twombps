/*
Package types containing the media type.
*/
package types

import (
	"os"
	"path"

	"gitlab.com/geo-gs/twombps/log"
	"gitlab.com/geo-gs/twombps/params"
)

// Media is the media container struct
type Media struct {
	name  string
	path  string
	video *Video
	audio *Audio
}

// GetMedia returns the media from the supplied path/title directory.
func GetMedia(input string) *Media {
	ext := path.Ext(input)
	isVideo := false
	for _, vExt := range params.VideoExtensions {
		if ext == ("." + vExt) {
			isVideo = true
			break
		}
	}
	if !isVideo {
		return nil
	}
	inputInfo, err := os.Stat(input)
	if err != nil || inputInfo.IsDir() {
		return nil
	}
	dir := path.Dir(input)
	base := path.Base(input)
	title := base[0 : len(base)-len(ext)]
	m := Media{}
	m.name = title
	m.path = dir
	m.video = &Video{}
	m.video.path = input
	return &m
}

// Name returns the media's name
func (m *Media) Name() string {
	return m.name
}

// Path returns the media's path
func (m *Media) Path() string {
	return m.path
}

// Video returns the media's video container
func (m *Media) Video() *Video {
	return m.video
}

// Audio returns the media's audio container
func (m *Media) Audio() *Audio {
	if m.audio != nil {
		return m.audio
	}
	m.audio = &Audio{}
	m.audio.path = m.video.path
	return m.audio
}

// MaxVideoBitrate returns the maximum video bitrate to use
func (m *Media) MaxVideoBitrate() int {
	return params.GetParameters().Bitrate() - m.Audio().MaxBitrate()
}

// OptimizedAudio returns true when the source's audio is considered optimized
func (m *Media) OptimizedAudio() bool {
	if m.Audio().Bitrate() < 10000 {
		return false
	}
	if m.Audio().Bitrate() > m.Audio().MaxBitrate() {
		return false
	}
	return true
}

// OptimizedVideo returns true when the source's video is considered optimized
func (m *Media) OptimizedVideo() bool {
	if m.Video().Bitrate() < 50000 {
		return false
	}
	if m.Video().Bitrate()+m.Audio().Bitrate() > params.GetParameters().Bitrate() {
		return false
	}
	if params.GetParameters().Force720p() && (m.Video().Width() > 1280 || m.Video().Height() > 720) {
		return false
	}
	if params.GetParameters().Force480p() && (m.Video().Width() > 720 || m.Video().Height() > 480) {
		return false
	}
	if params.GetParameters().ForceAv1() && m.Video().Codec() != "av1" {
		return false
	}
	if params.GetParameters().ForceAvc() && (m.Video().Codec() != "h264" || m.Video().Codec() != "x264" || m.Video().Codec() != "avc") {
		return false
	}
	if params.GetParameters().ForceHevc() && (m.Video().Codec() != "h265" || m.Video().Codec() != "x265" || m.Video().Codec() != "hevc") {
		return false
	}
	return true
}

// Optimized returns true when the source's video bitrate plus audio bitrate is less than the target bitrate
func (m *Media) Optimized() bool {
	return m.OptimizedVideo() && m.Video().Bitrate()+m.Audio().Bitrate() < params.GetParameters().Bitrate()
}

// Println prints metadata about the media
func (m *Media) Println() {
	log.Println("* Media", m.name)
	log.Println("  - name:", m.name)
	log.Println("  - path:", m.path)
	log.Println("  - optimized:", m.Optimized())
	log.Println("  * Video")
	log.Println("    - width:", m.Video().Width())
	log.Println("    - height:", m.Video().Height())
	log.Println("    - pix fmt:", m.Video().PixFmt())
	log.Println("    - color primaries:", m.Video().ColorPrimaries())
	log.Println("    - bitrate:", m.Video().Bitrate())
	log.Println("    - dar:", m.Video().Dar())
	log.Println("    - sar:", m.Video().Sar())
	log.Println("    * Crop")
	log.Println("      - filter:", m.Video().Crop().Filter())
	log.Println("      - width:", m.Video().Crop().Width())
	log.Println("      - height:", m.Video().Crop().Height())
	log.Println("    - duration:", m.Video().Duration())
	log.Println("    - frame rate:", m.Video().FrameRateStr())
	log.Println("    - progressive:", m.Video().Progressive())
	log.Println("  * Audio")
	log.Println("    - channels:", m.Audio().Channels())
	log.Println("    - bitrate:", m.Audio().Bitrate())
}
