# release-pkgs determine which packages are built for release and distrubtion
# when running a 'make release' command.
release-pkgs = \
	./cmd/twombps-cli \
	./cmd/twombps-gui
# all will build and install release binaries
all: release
# lint runs golangci-lint.
lint:
	golangci-lint run -c .golangci.yml ./...
# release builds and installs release binaries.
release:
	go install $(release-pkgs)

