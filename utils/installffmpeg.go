/*
Package utils containing logic to install FFMPEG when it does not exist.
*/
package utils

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"runtime"

	"gitlab.com/geo-gs/twombps/log"
)

// Returns the size of the latest ffmpeg binary in bytes.
func ffmpegSize() (int64, error) {
	var resp *http.Response
	var err error
	if runtime.GOOS == "darwin" && runtime.GOARCH == "amd64" {
		resp, err = http.Head("https://evermeet.cx/ffmpeg/get/zip")
	} else {
		return 0, fmt.Errorf("runtime %s %s is not supported", runtime.GOOS, runtime.GOARCH)
	}
	if err != nil {
		return 0, err
	}
	return resp.ContentLength, nil
}

// InstallFfmpeg downloads the latest ffmpeg binary to a temporary file without loading the whole file into memory.
func InstallFfmpeg(target string) error {
	size, err := ffmpegSize()
	if err != nil {
		return err
	}
	// Get the data
	var resp *http.Response
	if runtime.GOOS == "darwin" && runtime.GOARCH == "amd64" {
		resp, err = http.Get("https://evermeet.cx/ffmpeg/get/zip")
	} else {
		return fmt.Errorf("runtime %s %s is not supported", runtime.GOOS, runtime.GOARCH)
	}
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	// Create the file
	tmp, err := ioutil.TempFile(os.TempDir(), "ffmpeg")
	if err != nil {
		return err
	}
	defer os.Remove(tmp.Name())
	// Write the body to file
	i := 0
	for {
		i++
		_, err := io.CopyN(tmp, resp.Body, 1024)
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		if i%4096 == 0 {
			p := int(float64(i) * float64(1024) / float64(size) * float64(100))
			if p < 100 {
				log.Printf("Downloading FFmpeg snapshot: %d%%\n", p)
			}
		}
	}
	log.Println("Downloading FFmpeg snapshot: 100%")
	err = tmp.Close()
	if err != nil {
		return err
	}
	err = Unzip(tmp.Name(), target, "ffmpeg")
	if err != nil {
		return err
	}
	return nil
}
