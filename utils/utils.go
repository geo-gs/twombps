/*
Package utils containing convenience methods.
*/
package utils

import (
	"bufio"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"runtime"
	"strings"

	"gitlab.com/geo-gs/twombps/log"
	"gitlab.com/geo-gs/twombps/params"
	"gitlab.com/geo-gs/twombps/resources"
)

// GetBrand returns twombsp as a string
func GetBrand() string {
	return "twombsp"
}

// WorkingDirPath returns the path to the working directory.
func WorkingDirPath(name string) string {
	reg, _ := regexp.Compile("[^a-zA-Z0-9]+")
	processedName := reg.ReplaceAllString(name, "")
	path := filepath.Join(defaultMetadataDir(), processedName)
	if !PathExists(path) {
		Mkdir(path)
	}
	return path
}

// NnediWeightsPath returns the path to nnedi3_weights.bin.
func NnediWeightsPath() string {
	path := filepath.Join(defaultMetadataDir(), "nnedi3_weights.bin")
	if !PathExists(path) {
		os.WriteFile(path, resources.Nnedi3Weights(), 0o644) // #nosec
	}
	return path
}

// FfmpegPath returns the path to the FFmpeg binary.
func FfmpegPath() string {
	path := filepath.Join(defaultMetadataDir(), "ffmpeg")
	if !PathExists(path) {
		err := InstallFfmpeg(path)
		if err != nil {
			// hail mary: try using just the program name without path
			return "ffmpeg"
		}
	}
	return path
}

// FfprobePath returns the path to the FFProbe binary
func FfprobePath() string {
	path := filepath.Join(defaultMetadataDir(), "ffprobe")
	if !PathExists(path) {
		err := InstallFfprobe(path)
		if err != nil {
			// hail mary: try using just the program name without path
			return "ffprobe"
		}
	}
	return path
}

// defaultMetadataDir returns the default data directory of twombsp. The values for
// supported operating systems are:
//
// Linux:   $HOME/.twombps
// MacOS:   $HOME/Library/Application Support/Twombps
// Windows: %LOCALAPPDATA%\Twombps
//
// Might need to use "github.com/mitchellh/go-homedir" to get the home directory
func defaultMetadataDir() string {
	path := ""
	switch runtime.GOOS {
	case "windows":
		path = filepath.Join(os.Getenv("LOCALAPPDATA"), GetBrand())
	case "darwin":
		path = filepath.Join(os.Getenv("HOME"), "Library", "Application Support", GetBrand())
	default:
		path = filepath.Join(os.Getenv("HOME"), "."+strings.ToLower(GetBrand()))
	}
	if path != "" && !PathExists(path) {
		path = Mkdir(path)
	}
	return path
}

// PathExists returns true when the supplied path exists
func PathExists(path string) bool {
	if _, err := os.Stat(path); err == nil {
		return true
	}
	return false
}

// PrintFfprobe prints the supplied ffprobe parameters
func PrintFfprobe(ffprobeParams []string) {
	// return early when not debug mode
	if !params.GetParameters().Debug() {
		return
	}
	log.Print("ffprobe")
	for i, ffprobeParam := range ffprobeParams {
		if strings.HasPrefix(ffprobeParam, "-") || i+1 == len(ffprobeParams) {
			log.Print("\n  " + ffprobeParam)
		} else {
			log.Print(" " + ffprobeParam)
		}
	}
	log.Println("")
}

// PrintFfmpeg prints the supplied ffmpeg parameters
func PrintFfmpeg(ffmpegParams []string) {
	// return early when not debug mode
	if !params.GetParameters().Debug() {
		return
	}
	log.Print("ffmpeg")
	for i, ffmpegParam := range ffmpegParams {
		if strings.HasPrefix(ffmpegParam, "-") || i+1 == len(ffmpegParams) {
			log.Print(" \\\n  " + ffmpegParam)
		} else {
			log.Print(" \"" + ffmpegParam + "\"")
		}
	}
	log.Println("")
	log.Println("Executing...")
}

// Copy copies the contents of the source path to the destination
// path. The file will be created if it does not already exist. If
// the destination file exists, all it's contents will be replaced
// by the contents of the source file.
func Copy(src, dst string) error {
	in, err := os.Open(src) // #nosec
	if err != nil {
		return err
	}
	out, err := os.Create(dst) // #nosec
	if err != nil {
		return err
	}
	defer func() {
		cerr := out.Close()
		if err == nil {
			err = cerr
		}
	}()
	if _, err = io.Copy(out, in); err != nil {
		return err
	}
	err = out.Sync()
	if err != nil {
		return err
	}
	err = in.Close()
	return err
}

// Write creates a new file of the supplied content at the supplied target path and returns true if successful
func Write(target string, text string) bool {
	err := os.WriteFile(target, []byte(text), 0o644) // #nosec
	if err != nil {
		log.Printf("Failed to write to temporary file: %v\n", err)
		return false
	}
	return true
}

// WriteAll creates a new file of the supplied content at the supplied target path and returns true if successful
func WriteAll(target string, text []string) bool {
	return Write(target, strings.Join(text[:], "\n"))
}

// Mkdir created a directory and returns its path
func Mkdir(target string) string {
	err := os.MkdirAll(target, os.ModePerm)
	if err != nil {
		log.Printf("Failed to make directory: %v\n", err)
		return ""
	}
	return target
}

// MkTmpDir creates a temporary directory and returns its path
func MkTmpDir() string {
	tmpDir, err := ioutil.TempDir(os.TempDir(), GetBrand())
	if err != nil {
		log.Printf("Failed to make temporary directory: %v\n", err)
		return ""
	}
	return tmpDir
}

// Read returns the contents of the target file as an array of lines
func Read(target string) []string {
	file, err := os.Open(target) // #nosec
	if err != nil {
		log.Printf("Failed to open file: %s\n", target)
	}
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	var lines []string
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	err = file.Close()
	if err != nil {
		log.Println(err)
	}
	return lines
}
