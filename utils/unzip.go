/*
Package utils containing reusable logic to inflate a zip file.
*/
package utils

import (
	"archive/zip"
	"errors"
	"io"
	"os"
)

// Unzip the zip archive; move consensus.db to the destination.
// * src is the path to the zip archive to decompress.
// * dest is the path to where the file should be created
// * name is the name of the file inside the zip archive to decompress
func Unzip(src string, dest string, name string) error {
	_, err := os.Stat(dest)
	if !errors.Is(err, os.ErrNotExist) {
		err = os.Remove(dest)
		if err != nil {
			return err
		}
	}
	r, err := zip.OpenReader(src)
	if err != nil {
		return err
	}
	defer r.Close()
	for _, f := range r.File {
		if f.Name != name {
			continue
		}
		outFile, err := os.OpenFile(dest, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0755) // #nosec
		if err != nil {
			return err
		}
		rc, err := f.Open()
		if err != nil {
			return err
		}
		for {
			_, err := io.CopyN(outFile, rc, 1024)
			if err != nil {
				if err == io.EOF {
					break
				}
				return err
			}
		}
		// Close the file without defer to close before next iteration of loop
		err = outFile.Close()
		if err != nil {
			return err
		}
		err = rc.Close()
		if err != nil {
			return nil
		}
	}
	return nil
}
