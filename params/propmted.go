/*
Package params uses GUI prompts to populate the job's parameters.
*/
package params

import (
	"github.com/ncruces/zenity"
)

// FromPrompts gathers the params from a series of prompt dialogues.
func FromPrompts() *Parameters {
	params = &Parameters{
		bitrate:     2000000,
		cleanup:     false,
		cores:       0,
		force480p:   false,
		force720p:   false,
		force1080p:  false,
		force8Bit:   false,
		forceAv1:    false,
		forceAvc:    false,
		forceHevc:   false,
		debug:       false,
		dryRun:      false,
		skipCrop:    false,
		skipDecomb:  false,
		skipDenoise: false,
		skipNnedi:   false,
		help:        false,
		preset:      "slow",
	}
	// Set Input File
	err := params.promptInputs()
	if err != nil {
		return nil
	}
	// Set Output File
	err = params.promptOutput()
	if err != nil {
		return nil
	}
	// Set Video Encoder
	err = params.promptVideoEncoder()
	if err != nil {
		return nil
	}
	// Set Video Quality
	err = params.promptVideoQuality()
	if err != nil {
		return nil
	}
	// Set preset
	err = params.promptVisualEfficiency()
	if err != nil {
		return nil
	}
	return params
}

// promptInputs prompts the user to supply an input videos
func (p *Parameters) promptInputs() (err error) {
	patterns := []string{}
	for _, ext := range VideoExtensions {
		patterns = append(patterns, "*."+ext)
	}
	p.inputs, err = zenity.SelectFileMultiple(
		zenity.Title("Twombp Setup: Select Input"),
		zenity.ConfirmOverwrite(),
		zenity.Filename("movie.mkv"),
		zenity.FileFilters{
			{Name: "Video files", Patterns: patterns},
		})
	if len(p.Inputs()) != 0 {
		return
	}
	err = zenity.Error("No input video sources were supplied.",
		zenity.Title("Error: No Input"),
		zenity.ErrorIcon)
	p.promptInputs() // #nosec
	return
}

// promptOutput prompts the user to supply an output video
func (p *Parameters) promptOutput() (err error) {
	patterns := []string{}
	for _, ext := range VideoExtensions {
		patterns = append(patterns, "*."+ext)
	}
	p.output, err = zenity.SelectFileSave(
		zenity.Title("Twombp Setup: Select Output"),
		zenity.ConfirmOverwrite(),
		zenity.Filename(params.Inputs()[0]),
		zenity.FileFilters{
			{Name: "Video formats", Patterns: patterns},
		})
	if p.OutputFmt() != "" {
		return
	}
	err = zenity.Error("Supplied output video format is not supported.", // #nosec
		zenity.Title("Error: Unsupported Format"),
		zenity.ErrorIcon)
	p.promptOutput() // #nosec
	return
}

// promptVideoQuality prompts the user to select the video quality to target
func (p *Parameters) promptVideoQuality() (err error) {
	choice480pLow := "1 Mbps, 480p"
	choice480pHigh := "2 Mbps, 480p"
	choice720pLow := "3 Mbps, 720p"
	choice720pHigh := "4 Mbps 720p"
	choice1080pLow := "8 Mbps, 1080p"
	choice1080pHigh := "16 Mbps, 1080p"
	choiceMax := "Maximum"
	videoQuality, err := zenity.List(
		"Select desired video quality from the list below.",
		[]string{choice480pLow, choice480pHigh, choice720pLow, choice720pHigh, choice1080pLow, choice1080pHigh, choiceMax},
		zenity.Title("Twombp Setup: Select Video Quality"),
		zenity.DefaultItems(choice480pHigh),
		zenity.DisallowEmpty(),
	)
	if err != nil {
		return
	}
	if videoQuality == choice480pLow {
		p.bitrate = 1000000
		p.force480p = true
	} else if videoQuality == choice480pHigh {
		p.bitrate = 2000000
		p.force480p = true
	} else if videoQuality == choice720pLow {
		p.bitrate = 3000000
		p.force720p = true
	} else if videoQuality == choice720pHigh {
		p.bitrate = 4000000
		p.force720p = true
	} else if videoQuality == choice1080pLow {
		p.bitrate = 8000000
		p.force1080p = true
	} else if videoQuality == choice1080pHigh {
		p.bitrate = 16000000
		p.force1080p = true
	} else if videoQuality == choiceMax {
		p.bitrate = 99999999
	}
	return
}

// promptVideoEncoder prompts the user to select the video encoder to use
func (p *Parameters) promptVideoEncoder() (err error) {
	choice8bitH264 := "H.264 8-bit"
	choice10bitH265 := "H.265 10-bit"
	choice10bitAv1 := "AV1 10-bit"
	videoEncoder, err := zenity.List(
		"Select desired video encoder from the list below.",
		[]string{choice8bitH264, choice10bitH265, choice10bitAv1},
		zenity.Title("Twombp Setup: Select Video Encoder"),
		zenity.DefaultItems(choice10bitAv1),
		zenity.DisallowEmpty(),
	)
	if err != nil {
		return
	}
	if videoEncoder == choice8bitH264 {
		p.force8Bit = true
		p.forceAvc = true
	} else if videoEncoder == choice10bitH265 {
		p.forceHevc = true
	} else if videoEncoder == choice10bitAv1 {
		p.forceAv1 = true
	}
	return
}

// promptVisualEfficiency prompts the user to select the how efficient the visual quality should be
func (p *Parameters) promptVisualEfficiency() (err error) {
	choiceGood := "good"
	choiceFair := "fair"
	choicePoor := "poor"
	compressionQuality, err := zenity.List(
		"Select desired visual efficiency from the list below. Better efficiency will produce a better visual for a fixed number of bits but will also take longer to encode.",
		[]string{choiceGood, choiceFair, choicePoor},
		zenity.Title("Twombp Setup: Select Visual Efficiency"),
		zenity.DefaultItems(choiceGood),
		zenity.DisallowEmpty(),
	)
	if err != nil {
		return
	}
	if compressionQuality == choiceGood {
		p.preset = "slow"
	} else if compressionQuality == choiceFair {
		p.preset = "fast"
	} else if compressionQuality == choicePoor {
		p.preset = "veryfast"
	}
	return
}
