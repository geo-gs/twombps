/*
Package params containing the logic to parse CLI arguments into the job's parameters.
*/
package params

import (
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strings"

	"gitlab.com/geo-gs/twombps/log"
)

// ParseFlags defines and parses the parameters.
func ParseFlags() *Parameters {
	bitrarePtr := flag.Int("bitrate", -1, "Maximum bitrate of the resulting video.")
	cleanupPtr := flag.Bool("cleanup", false, "Supply this flag when the original videos should be discarded.")
	coresPtr := flag.Int("cores", 0, "Number of CPU cores to use to encode the video. Defaults to one less than the total number of CPU cores.")
	debugPtr := flag.Bool("debug", false, "Supply this flag when detailed logs should be printed.")
	dryRunPtr := flag.Bool("dryRun", false, "Supply this flag when the video encoding step should be skipped.")
	force480pPtr := flag.Bool("force480p", false, "Supply this flag when the resulting video's resolution should be 480p.")
	force720pPtr := flag.Bool("force720p", false, "Supply this flag when the resulting video's resolution should be 720p.")
	force1080pPtr := flag.Bool("force1080p", false, "Supply this flag when the resulting video's resolution should be 1080p.")
	force8BitPtr := flag.Bool("force8Bit", false, "Supply this flag when the resulting video's color depth should be 8-bit instead of 10-bit.")
	forceAv1Ptr := flag.Bool("forceAv1", false, "Supply this flag when the resulting video's codec must be AV1.")
	forceAvcPtr := flag.Bool("forceAvc", false, "Supply this flag when the resulting video's codec must be AVC.")
	forceHevcPtr := flag.Bool("forceHevc", false, "Supply this flag when the resulting video's codec must be HEVC.")
	inputPtr := flag.String("input", "", "The path to the input.")
	outputPtr := flag.String("output", "", "The path to the output. Defaults to input.")
	presetPtr := flag.String("preset", "slow", "The preset to use. Slower preset values will produce better video quality. Valid preset values are:"+PresetValues)
	skipCropPtr := flag.Bool("skipCrop", false, "Supply this flag when letter-box bars in the source video should not be removed.")
	skipDecombPtr := flag.Bool("skipDecomb", false, "Supply this flag when interlaced video should not be converted to progressive video.")
	skipDenoisePtr := flag.Bool("skipDenoise", false, "Supply this flag when the denoiser should not be used before scaling the video.")
	skipNnediPtr := flag.Bool("skipNnedi", false, "Supply this flag when the nnedi upscaler not be used to scale the video.")
	flag.Parse()
	params = &Parameters{}
	params.bitrate = *bitrarePtr
	params.cleanup = *cleanupPtr
	params.cores = *coresPtr
	params.debug = *debugPtr
	params.dryRun = *dryRunPtr
	params.force480p = *force480pPtr
	params.force720p = *force720pPtr
	params.force1080p = *force1080pPtr
	params.force8Bit = *force8BitPtr
	params.forceAv1 = *forceAv1Ptr
	params.forceAvc = *forceAvcPtr
	params.forceHevc = *forceHevcPtr
	err := params.parseInput(*inputPtr)
	if err != nil {
		log.Println(err)
		return nil
	}
	err = params.parseOutput(*outputPtr)
	if err != nil {
		log.Println(err)
		return nil
	}
	params.preset = *presetPtr
	params.skipCrop = *skipCropPtr
	params.skipDecomb = *skipDecombPtr
	params.skipDenoise = *skipDenoisePtr
	params.skipNnedi = *skipNnediPtr
	return params
}

// parseInputs parses the user to supply an input video or directory of videos
func (p *Parameters) parseInput(input string) (err error) {
	inputInfo, err := os.Stat(input)
	if err != nil {
		return
	}
	if !inputInfo.IsDir() {
		p.inputs = []string{input}
		return
	}
	files, err := ioutil.ReadDir(input)
	if err != nil {
		return
	}
	for _, file := range files {
		for _, extension := range VideoExtensions {
			suffix := fmt.Sprintf(".%s", extension)
			if strings.HasSuffix(file.Name(), suffix) {
				p.inputs = append(p.inputs, path.Join(input, file.Name()))
			}
		}
	}
	return
}

// promptOutput prompts the user to supply an output video
func (p *Parameters) parseOutput(output string) (err error) {
	p.output = output
	if p.OutputFmt() == "" {
		err = errors.New("supplied output video format is not supported")
	}
	return
}
