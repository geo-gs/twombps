/*
Package params containing the job's configured parameters.
*/
package params

import (
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"strings"

	"gitlab.com/geo-gs/twombps/log"
)

// VideoExtensions is a string array of all the supported video extensions
var VideoExtensions = []string{"mp4", "mkv", "webm"}

// PresetValues is a string of all the preset values
var PresetValues = " placebo veryslow slower slow medium fast faster veryfast superfast ultrafast "
var params *Parameters

// Parameters is the parameters struct
type Parameters struct {
	inputs      []string
	output      string
	bitrate     int
	cleanup     bool
	cores       int
	force480p   bool
	force720p   bool
	force1080p  bool
	force8Bit   bool
	forceAv1    bool
	forceAvc    bool
	forceHevc   bool
	debug       bool
	dryRun      bool
	skipCrop    bool
	skipDecomb  bool
	skipDenoise bool
	skipNnedi   bool
	help        bool
	preset      string
}

// GetParameters returns the parameters
func GetParameters() *Parameters {
	return params
}

// Println prints the parameters
func (p *Parameters) Println() {
	log.Println("inputs:", p.inputs)
	log.Println("output:", p.output)
	log.Println("bitrate:", p.Bitrate())
	log.Println("cores:", p.Cores())
	log.Println("cleanup:", p.cleanup)
	log.Println("force8Bit:", p.force8Bit)
	log.Println("forceAvc:", p.forceAvc)
	log.Println("forceAv1:", p.forceAv1)
	log.Println("force480p:", p.force480p)
	log.Println("force720p:", p.force720p)
	log.Println("debug:", p.debug)
	log.Println("dryRun:", p.dryRun)
	log.Println("skipDenoise:", p.skipDenoise)
	log.Println("skipDecomb:", p.skipDecomb)
	log.Println("skipCrop:", p.skipCrop)
	log.Println("preset:", p.preset)
}

// Inputs returns the input path
func (p *Parameters) Inputs() []string {
	return p.inputs
}

// Output returns the output path
func (p *Parameters) Output() string {
	return p.output
}

// OutputFmt returns the output format
func (p *Parameters) OutputFmt() string {
	ext := path.Ext(p.output)
	for _, vExt := range VideoExtensions {
		if ext == ("." + vExt) {
			return vExt
		}
	}
	return ""
}

// OutputDir returns all but the last element of the output path, typically the output path's directory.
// After dropping the final element using Split, the path is Cleaned and trailing
// slashes are removed.
// If the path is empty, Dir returns ".".
// If the path consists entirely of slashes followed by non-slash bytes, Dir
// returns a single slash. In any other case, the returned path does not end in a
// slash.
func (p *Parameters) OutputDir() string {
	return path.Dir(p.output)
}

// OutputBase returns the last element of the output path.
// Trailing slashes are removed before extracting the last element.
// If the path is empty, Base returns ".".
// If the path consists entirely of slashes, Base returns "/".
func (p *Parameters) OutputBase() string {
	return path.Base(p.output)
}

// Bitrate returns the max bitrate limit
func (p *Parameters) Bitrate() int {
	if p.bitrate > 0 {
		return p.bitrate
	}
	return 2000000
}

// Cores returns the number of CPU cores to use
func (p *Parameters) Cores() int {
	if p.cores < 1 {
		p.cores = runtime.NumCPU() - 1
	}
	if p.cores < 1 {
		p.cores = 1
	}
	runtime.GOMAXPROCS(p.cores)
	return p.cores
}

// Force8Bit returns true when the force8Bit parameter is supplied
func (p *Parameters) Force8Bit() bool {
	return p.force8Bit
}

// ForceAvc returns true when the forceAvc parameter is supplied
func (p *Parameters) ForceAvc() bool {
	return p.forceAvc
}

// ForceHevc returns true when the forceHevc parameter is supplied
func (p *Parameters) ForceHevc() bool {
	return p.forceHevc
}

// ForceAv1 returns true when the forceAv1 parameter is supplied
func (p *Parameters) ForceAv1() bool {
	return p.forceAv1
}

// Force480p returns true when the force480p parameter is supplied
func (p *Parameters) Force480p() bool {
	return p.force480p
}

// Force720p returns true when the force720p parameter is supplied
func (p *Parameters) Force720p() bool {
	return p.force720p
}

// Force1080p returns true when the force1080p parameter is supplied
func (p *Parameters) Force1080p() bool {
	return p.force1080p
}

// Debug returns true when the debug parameter is supplied
func (p *Parameters) Debug() bool {
	return p.debug
}

// DryRun returns true when the dryRun parameter is supplied
func (p *Parameters) DryRun() bool {
	return p.dryRun
}

// Denoise returns true when the skipDenoise parameter is not supplied
func (p *Parameters) Denoise() bool {
	return !p.skipDenoise
}

// Decomb returns true when the skipDecomb parameter is not supplied
func (p *Parameters) Decomb() bool {
	return !p.skipDecomb
}

// Crop returns true when the skipCrop parameter is not supplied
func (p *Parameters) Crop() bool {
	return !p.skipCrop
}

// Nnedi returns true when the skipNnedi parameter is not supplied
func (p *Parameters) Nnedi() bool {
	return !p.skipNnedi
}

// Help returns true when the help parameter is supplied
func (p *Parameters) Help() bool {
	return p.help
}

// Ultrafast returns true when the preset is set to ultrfast
func (p *Parameters) Ultrafast() bool {
	return p.preset == "ultrafast"
}

// Preset returns the preset
func (p *Parameters) Preset() string {
	// TODO: add sanity check
	return p.preset
}

// Valid returns true when the configured preset group is valid
func (p *Parameters) Valid() bool {
	if len(p.inputs) == 0 {
		log.Println("INVALID: missing input videos")
		return false
	}
	if !strings.Contains(PresetValues, " "+p.preset+" ") {
		log.Println("INVALID: illegal preset", p.preset)
		return false
	}
	return true
}

// PresetGroup returns the preset group to use
func (p *Parameters) PresetGroup() int {
	switch " " + p.preset + " " {
	case " ultrafast ", " superfast ", " veryfast ", " faster ":
		return 0
	case " fast ", " medium ", " slow ":
		return 1
	case " slower ", " veryslow ", " placebo ":
		return 2
	default:
		return -1
	}
}

// PresetInt returns the integer values corresponding to the preset param.
// Values are from 0 to 8 inclusive.
func (p *Parameters) PresetInt() int {
	switch " " + p.preset + " " {
	case " ultrafast ":
		return 8
	case " superfast ":
		return 7
	case " veryfast ":
		return 7
	case " faster ":
		return 6
	case " fast ":
		return 5
	case " medium ":
		return 4
	case " slow ":
		return 3
	case " slower ":
		return 2
	case " veryslow ":
		return 1
	case " placebo ":
		return 0
	default:
		return -1
	}
}

// Crf returns the CRF value to use.
// Accordig to https://goughlui.com/2016/08/27/video-compression-testing-x264-vs-x265-crf-in-handbrake-0-10-5/
// there’s really no big difference between PSNRs for the average case between x264 and x265 on a CRF value basis.
// Also, according to https://www.reddit.com/r/AV1/comments/t59j32/encoder_tuning_part_4_a_2nd_generation_guide_to/
// there is not a big difference between x265 and AV1 on a CRF value basis.
func (p *Parameters) Crf() int {
	return 16
}

// ScalingAlgo returns the scaline algorithm to use
func (p *Parameters) ScalingAlgo() string {
	if p.PresetGroup() == 0 {
		return "bicubic"
	}
	return "spline"
}

// VideoProfile returns the video profile to use
func (p *Parameters) VideoProfile() string {
	if p.force8Bit {
		return "main"
	} else if p.forceAvc {
		return "high10"
	} else {
		return "main10"
	}
}

// VideoCodec returns the video codec to use
func (p *Parameters) VideoCodec() string {
	if p.forceAvc == true {
		return "libx264"
	} else if p.forceAv1 == true {
		return "libaom-av1"
	} else {
		return "libx265"
	}
}

// Cleanup removes all the original files from the path unless the skip cleanup parameter is set to true
func (p *Parameters) Cleanup(path string) {
	if !p.cleanup {
		return
	}
	files, err := ioutil.ReadDir(path)
	if err != nil {
		log.Println(err)
		return
	}
	for _, file := range files {
		if file.Name() == "orig" {
			log.Println("Discarding original files stored in:", filepath.Join(path, file.Name()))
			err = os.RemoveAll(filepath.Join(path, file.Name()))
			if err != nil {
				log.Println(err)
			}
		} else if strings.HasSuffix(file.Name(), ".orig") {
			log.Println("Discarding original file:", filepath.Join(path, file.Name()))
			err = os.Remove(filepath.Join(path, file.Name()))
			if err != nil {
				log.Println(err)
			}
		}
	}
}
