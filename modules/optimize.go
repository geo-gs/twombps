/*
Package modules containing the logic to optimize the video.
*/
package modules

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"

	"gitlab.com/geo-gs/twombps/log"
	"gitlab.com/geo-gs/twombps/modules/optimize/video/codecs"
	"gitlab.com/geo-gs/twombps/modules/optimize/video/filters"
	"gitlab.com/geo-gs/twombps/params"
	"gitlab.com/geo-gs/twombps/types"
	"gitlab.com/geo-gs/twombps/utils"
)

// Optimize will optimize the supplied media
func Optimize(name, videoPath, audioPath string) (string, error) {
	optimizedAudioPath, err := optimizeAudio(name, audioPath)
	if err != nil {
		log.Println("Failed to optimize audio.")
		return "", err
	}
	audio := &types.Audio{}
	audio.SetPath(optimizedAudioPath)
	maxVideoBitrate := params.GetParameters().Bitrate() - audio.Bitrate()
	optimizedVideoPath, err := optimizeVideo(name, videoPath, maxVideoBitrate)
	if err != nil {
		log.Println("Failed to scale video.")
		return "", err
	}
	scoredVideoPath, err := scoreVideo(name, optimizedVideoPath, audioPath)
	if err != nil {
		log.Println("Failed to score scaled video.")
		return "", err
	}
	return scoredVideoPath, nil
}

func optimizeAudio(name, audioPath string) (string, error) {
	path := utils.WorkingDirPath(name)
	targetFile := filepath.Join(path, "optimized.mka")
	if utils.PathExists(targetFile) {
		return targetFile, nil
	}
	log.Println("Optimize Audio...")
	tmpFile := filepath.Join(utils.MkTmpDir(), "audio.mka")
	audio := &types.Audio{}
	audio.SetPath(audioPath)
	ffmpegParams := []string{}
	ffmpegParams = append(ffmpegParams, "-i", audioPath)
	ffmpegParams = append(ffmpegParams, "-vn")
	ffmpegParams = append(ffmpegParams, "-map", "0:a:0")
	ffmpegParams = append(ffmpegParams, "-c:a", "libopus")
	ffmpegParams = append(ffmpegParams, "-filter:a", "aresample=async=1:min_hard_comp=0.100000:first_pts=0")
	ffmpegParams = append(ffmpegParams, "-ab", strconv.Itoa(audio.MaxBitrate()))
	ffmpegParams = append(ffmpegParams, "-ac", strconv.Itoa(audio.Channels()))
	ffmpegParams = append(ffmpegParams, "-y")
	ffmpegParams = append(ffmpegParams, tmpFile)
	utils.PrintFfmpeg(ffmpegParams)
	err := exec.Command(utils.FfmpegPath(), ffmpegParams...).Run() // #nosec
	if err != nil {
		log.Println("Unable to execute: ffmpeg", strings.Join(ffmpegParams, " "))
		return "", err
	}
	err = os.Rename(tmpFile, targetFile)
	if err != nil {
		return "", err
	}
	return targetFile, nil
}

func scoreVideo(name, videoPath, audioPath string) (string, error) {
	path := utils.WorkingDirPath(name)
	fileName := "score_scaled." + params.GetParameters().OutputFmt()
	targetFile := filepath.Join(path, fileName)
	if utils.PathExists(targetFile) {
		log.Printf("### %s scaled video has already been scored.\n", name)
		return targetFile, nil
	}
	log.Println("Score Scaled Video:")
	tmpFile := filepath.Join(utils.MkTmpDir(), fileName)
	ffmpegParams := []string{}
	ffmpegParams = append(ffmpegParams, "-i", videoPath)
	ffmpegParams = append(ffmpegParams, "-i", audioPath)
	ffmpegParams = append(ffmpegParams, "-map", "0:v:0")
	ffmpegParams = append(ffmpegParams, "-c:v", "copy")
	ffmpegParams = append(ffmpegParams, "-map", "1:a:0")
	ffmpegParams = append(ffmpegParams, "-c:a", "copy")
	ffmpegParams = append(ffmpegParams, "-movflags", "+faststart")
	// Ffmpeg can automatically determine the appropriate format from the output file name.
	// So, no need to define the -f option
	ffmpegParams = append(ffmpegParams, "-y")
	ffmpegParams = append(ffmpegParams, tmpFile)
	utils.PrintFfmpeg(ffmpegParams)
	err := exec.Command(utils.FfmpegPath(), ffmpegParams...).Run() // #nosec
	if err != nil {
		log.Println("Unable to execute: ffmpeg", strings.Join(ffmpegParams, " "))
		return "", err
	}
	err = os.Rename(tmpFile, targetFile)
	if err != nil {
		return "", err
	}
	return targetFile, nil
}

func optimizeVideo(name, videoPath string, maxBitrate int) (string, error) {
	path := utils.WorkingDirPath(name)
	targetFile := filepath.Join(path, "scaled.mkv")
	if utils.PathExists(targetFile) {
		log.Printf("### %s has already been scaled.\n", name)
		return targetFile, nil
	}
	log.Println("Scaling Video...")
	original := &types.Video{}
	original.SetPath(filepath.Join(path, "original.mkv"))
	if !utils.PathExists(original.Path()) {
		err := utils.Copy(videoPath, original.Path())
		if err != nil {
			return "", err
		}
	}
	original.DetectCrop()
	scenes := ""
	for _, scene := range splitScenes(name, original.Path(), maxBitrate) {
		scenes = scenes + fmt.Sprintf("file '%v'\n", scene.Path())
	}
	utils.Write(filepath.Join(path, "scenes.txt"), scenes)
	tmpFile := filepath.Join(utils.MkTmpDir(), "video.mkv")
	ffmpegParams := []string{}
	ffmpegParams = append(ffmpegParams, "-f", "concat")
	ffmpegParams = append(ffmpegParams, "-safe", "0")
	ffmpegParams = append(ffmpegParams, "-i", filepath.Join(path, "scenes.txt"))
	ffmpegParams = append(ffmpegParams, "-c", "copy")
	ffmpegParams = append(ffmpegParams, tmpFile)
	utils.PrintFfmpeg(ffmpegParams)
	err := exec.Command(utils.FfmpegPath(), ffmpegParams...).Run() // #nosec
	if err != nil {
		log.Println("Unable to execute: ffmpeg", strings.Join(ffmpegParams, " "))
		return "", err
	}
	optimized := &types.Video{}
	optimized.SetPath(tmpFile)
	log.Println("Optimized duration:", optimized.Duration())
	log.Println("Original duration:", original.Duration())
	err = os.Rename(tmpFile, targetFile)
	if err != nil {
		return "", err
	}
	return targetFile, nil
}

func splitScenes(name, videoPath string, maxBitrate int) []*types.Video {
	video := &types.Video{}
	video.SetPath(videoPath)
	progressive := video.Progressive()
	detectedScenesPath := filepath.Join(utils.WorkingDirPath(name), "detected_scenes.txt")
	if !utils.PathExists(detectedScenesPath) {
		scenes, err := video.DetectScenes()
		if err != nil {
			log.Printf("Unable to detect scenes due to: %v", err)
			return nil
		}
		if !utils.WriteAll(detectedScenesPath, scenes) {
			log.Printf("Unable to write detected scenes file")
			return nil
		}
	}
	cores := params.GetParameters().Cores()
	minDuration := float64(120)
	videoDuration, _ := strconv.ParseFloat(video.Duration(), 64)
	if videoDuration/float64(cores) < minDuration {
		minDuration = videoDuration / float64(cores)
	}
	prevTime := float64(0)
	detectedTimes := make([]string, 0)
	for _, detectedScene := range utils.Read(detectedScenesPath) {
		meta := strings.Split(detectedScene, "|")
		time := strings.TrimPrefix(meta[0], "best_effort_timestamp_time=")
		if thisTime, _ := strconv.ParseFloat(time, 64); thisTime-prevTime > minDuration || thisTime > videoDuration-minDuration {
			detectedTimes = append(detectedTimes, time)
			log.Println("scene", len(detectedTimes), time)
			prevTime = thisTime
		}
	}
	detectedTimes = append(detectedTimes, video.Duration())
	log.Println("scene", len(detectedTimes), video.Duration())
	// Create a bounded channel, limit that channel to the supplie dnumber of cores.
	// source: https://medium.com/@deckarep/gos-extended-concurrency-semaphores-part-1-5eeabfa351ce
	sem := make(chan int, cores)
	start := "0"
	scenes := make([]*types.Video, 0)
	for i, end := range detectedTimes {
		scene := types.Video{}
		scene.SetPath(videoPath + ".pt" + strconv.Itoa(i))
		scenes = append(scenes, &scene)
		sem <- 1
		go func(videoPath string, srcWidth, srcHeight int, crop *types.Crop, progressive bool, frameRate float64, frameRateStr, dar, srcPixFmt, srcColorPrimaries, start, end string, maxBitrate, i int) {
			targetFile := videoPath + ".pt" + strconv.Itoa(i)
			if !utils.PathExists(targetFile) {
				clip64, retime64, err := analysisScene(videoPath, frameRateStr, start, end, frameRate, i)
				if err != nil {
					log.Println(err)
				}
				optimizeScenePath, err := optimizeScene(videoPath, srcWidth, srcHeight, crop, progressive, frameRateStr, dar, srcPixFmt, srcColorPrimaries, start, end, maxBitrate, i, clip64, retime64)
				if err != nil {
					log.Println(err)
				}
				err = os.Rename(optimizeScenePath, targetFile)
				if err != nil {
					log.Println(err)
				}
			}
			<-sem
		}(videoPath, video.Width(), video.Height(), video.Crop(), progressive, video.FrameRate(), video.FrameRateStr(), video.Dar(), video.PixFmt(), video.ColorPrimaries(), start, end, maxBitrate, i)
		start = end
	}
	// Fill the bounded channel up which forces us to block until all threads have finished.
	for i := 0; i < cores; i++ {
		sem <- 1
	}
	// Clear the bounded channel. Probably not needed.
	for i := 0; i < cores; i++ {
		<-sem
	}
	log.Println("Finished optimizig scenes.")
	return scenes
}

func analysisScene(videoPath, frameRateStr, start, end string, frameRate float64, count int) (clip64, retime64 float64, err error) {
	tmpFile := filepath.Join(utils.MkTmpDir(), "scene.mkv")
	log.Println("Analysis scene", count)
	video := &types.Video{}
	video.SetPath(videoPath)
	ffmpegParams := []string{}
	// https://stackoverflow.com/questions/21420296/how-to-extract-time-accurate-video-segments-with-ffmpeg
	// Just supplying the -ss and -to parameters before the input value to enable keyframe seeking
	// should be fince since we are obtaining the times to split on by finding keyframes with large
	// amounts change from the previous frame.
	ffmpegParams = append(ffmpegParams, "-ss", start)
	ffmpegParams = append(ffmpegParams, "-to", end)
	ffmpegParams = append(ffmpegParams, "-i", videoPath)
	// Do not copy the metadata to the scene
	ffmpegParams = append(ffmpegParams, "-map_metadata", "-1")
	ffmpegParams = append(ffmpegParams, "-map", "0:v:0")
	ffmpegParams = append(ffmpegParams, "-map", "-0:t") // remove attachments
	// Setting vsync to cfr forces a constant frame rate by duplicating and dropping frames.
	ffmpegParams = append(ffmpegParams, "-vsync", "cfr")
	// Limit threads here; we are using parallel processing of multiple scenes instead of
	// ffmpeg's native multithreading. This is needed because a lot of the video filters
	// are slow because they are not multi-threaded.
	ffmpegParams = append(ffmpegParams, "-threads", "1")
	ffmpegParams = append(ffmpegParams, "-vcodec", "libx264")
	ffmpegParams = append(ffmpegParams, "-r", video.FrameRateStr())
	// crf is set to 10 when preset is slow.
	ffmpegParams = append(ffmpegParams, "-crf", "40")
	ffmpegParams = append(ffmpegParams, "-preset", "ultrafast")
	ffmpegParams = append(ffmpegParams, "-g", "60")
	ffmpegParams = append(ffmpegParams, "-sc_threshold", "0")
	// Do not include audio at this time
	ffmpegParams = append(ffmpegParams, "-an")
	ffmpegParams = append(ffmpegParams, "-y")
	ffmpegParams = append(ffmpegParams, tmpFile)
	utils.PrintFfmpeg(ffmpegParams)
	output, err := exec.Command(utils.FfmpegPath(), ffmpegParams...).CombinedOutput() // #nosec
	if err != nil {
		log.Println("Unable to execute: ffmpeg", strings.Join(ffmpegParams, " "))
		err = errors.New(string(output))
		return
	}
	scene := &types.Video{}
	scene.SetPath(tmpFile)
	actualSceneDuration64 := scene.Duration64()
	err = os.Remove(tmpFile)
	if err != nil {
		log.Println(err)
	}
	start64, _ := strconv.ParseFloat(start, 64)
	end64, _ := strconv.ParseFloat(end, 64)
	expectedSceneDuration64 := end64 - start64
	difference64 := expectedSceneDuration64 - actualSceneDuration64
	frameDuration64 := float64(1) / frameRate
	if difference64 >= frameDuration64 && difference64 < float64(2)*frameDuration64 {
		log.Println("***Scene", count, "is", difference64, "seconds too long; clip start of scene.")
		clip64 = difference64
		return
	}
	if difference64 >= float64(2)*frameDuration64 {
		log.Println("***Scene", count, "is", difference64, "seconds too long; retime scene.")
		retime64 = (actualSceneDuration64 / expectedSceneDuration64)
		return
	} else if difference64 <= float64(-1)*frameDuration64 {
		log.Println("***Scene", count, "is", difference64, "seconds too short; retime scene.")
		retime64 = (actualSceneDuration64 / expectedSceneDuration64)
		return
	}
	return
}

func optimizeScene(videoPath string, srcWidth, srcHeight int, crop *types.Crop, progressive bool, frameRateStr, dar, srcPixFmt, srcColorPrimaries, start, end string, maxBitrate, count int, clip64, retime64 float64) (string, error) {
	start64, _ := strconv.ParseFloat(start, 64)
	end64, _ := strconv.ParseFloat(end, 64)
	tmpFile := filepath.Join(utils.MkTmpDir(), "scene.mkv")
	log.Println("Optimizing scene", count)
	video := &types.Video{}
	video.SetPath(videoPath)
	ffmpegParams := []string{}
	// https://stackoverflow.com/questions/21420296/how-to-extract-time-accurate-video-segments-with-ffmpeg
	// Just supplying the -ss and -to parameters before the input value to enable keyframe seeking
	// should be fince since we are obtaining the times to split on by finding keyframes with large
	// amounts change from the previous frame.
	ffmpegParams = append(ffmpegParams, "-ss", fmt.Sprintf("%f", (start64+clip64)))
	ffmpegParams = append(ffmpegParams, "-to", fmt.Sprintf("%f", end64))
	ffmpegParams = append(ffmpegParams, "-i", videoPath)
	// Do not copy the metadata to the scene because it can create issues when trying to concatinate the
	// scenes back together.
	ffmpegParams = append(ffmpegParams, "-map_metadata", "-1")
	ffmpegParams = append(ffmpegParams, "-map", "0:v:0")
	ffmpegParams = append(ffmpegParams, "-map", "-0:t") // remove attachments
	// Add video filters such as scaling, denoising, and deinterlacing.
	var videoFilters string
	if params.GetParameters().Force480p() {
		videoFilters = filters.Vf480p(srcWidth, srcHeight, crop, frameRateStr, dar, srcPixFmt, srcColorPrimaries, progressive)
	} else if params.GetParameters().Force720p() {
		videoFilters = filters.Vf720p(srcWidth, srcHeight, crop, frameRateStr, dar, srcPixFmt, srcColorPrimaries, progressive)
	} else if params.GetParameters().Force1080p() {
		videoFilters = filters.Vf1080p(srcWidth, srcHeight, crop, frameRateStr, dar, srcPixFmt, srcColorPrimaries, progressive)
	} else {
		videoFilters = filters.VfNoResize(srcWidth, srcHeight, crop, frameRateStr, srcPixFmt, progressive)
	}
	if retime64 > 0 {
		videoFilters = fmt.Sprintf("setpts=%f*PTS,", retime64) + videoFilters
	}
	ffmpegParams = append(ffmpegParams, "-vf", videoFilters)
	// Setting vsync to cfr forces a constant frame rate by duplicating and dropping frames.
	ffmpegParams = append(ffmpegParams, "-fps_mode:v", "cfr")
	// Limit threads here; we are using parallel processing of multiple scenes instead of
	// ffmpeg's native multithreading. This is needed because a lot of the video filters
	// are slow because they are not multi-threaded.
	ffmpegParams = append(ffmpegParams, "-threads", "1")
	ffmpegParams = append(ffmpegParams, "-vcodec", params.GetParameters().VideoCodec())
	ffmpegParams = append(ffmpegParams, "-r", video.FrameRateStr())
	ffmpegParams = append(ffmpegParams, "-crf", strconv.Itoa(params.GetParameters().Crf()))
	if params.GetParameters().ForceAvc() {
		ffmpegParams = append(ffmpegParams, codecs.AvcParams(video.FrameRate())...)
	} else if params.GetParameters().ForceAv1() {
		ffmpegParams = append(ffmpegParams, codecs.Av1Params(video.FrameRate(), maxBitrate)...)
	} else {
		ffmpegParams = append(ffmpegParams, codecs.HevcParams(video.FrameRate())...)
	}
	// Setting the maxrate parameter caps to 50kbps less than the maximum bitrate.
	// This gives us a little bit of wiggle room to stay under the cap without needing to do two passes.
	ffmpegParams = append(ffmpegParams, "-maxrate", strconv.Itoa(maxBitrate-50000))
	// Cap max buffer size to twice the max bitrate.
	// Having a buffer allows us to increase our quality as we are able to pre-load the buffer
	// with additional quality whenever the bitrate is not maxed.
	ffmpegParams = append(ffmpegParams, "-bufsize", strconv.Itoa(maxBitrate*2))
	ffmpegParams = append(ffmpegParams, "-f", "mp4")
	ffmpegParams = append(ffmpegParams, "-y")
	ffmpegParams = append(ffmpegParams, tmpFile)
	utils.PrintFfmpeg(ffmpegParams)
	err := exec.Command(utils.FfmpegPath(), ffmpegParams...).Run() // #nosec
	if err != nil {
		log.Println("Unable to execute: ffmpeg", strings.Join(ffmpegParams, " "))
		return "", err
	}
	return tmpFile, nil
}
