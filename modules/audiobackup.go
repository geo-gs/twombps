/*
Package modules containing the logic to back up the audio.
*/
package modules

import (
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"

	"gitlab.com/geo-gs/twombps/log"
	"gitlab.com/geo-gs/twombps/params"
	"gitlab.com/geo-gs/twombps/types"
	"gitlab.com/geo-gs/twombps/utils"
)

// AudioBackup backs up the supplied metia's audio.
func AudioBackup(m *types.Media) (string, error) {
	path := utils.WorkingDirPath(m.Name())
	archiveAudioPath := filepath.Join(params.GetParameters().OutputDir(), m.Name()+".mka.orig")
	targetAudioPath := filepath.Join(path, "original.mka")
	if utils.PathExists(targetAudioPath) {
		log.Printf("### %s has already had its audio backed up.\n", m.Name())
		return targetAudioPath, nil
	} else if utils.PathExists(archiveAudioPath) {
		err := utils.Copy(archiveAudioPath, targetAudioPath)
		if err == nil {
			log.Printf("### %s has already had its audio backed up.\n", m.Name())
			return targetAudioPath, nil
		}
	}
	log.Printf("### Backup Audio for %s.\n", m.Name())
	sourceVideoPath := filepath.Join(path, "original.mkv")
	if !utils.PathExists(sourceVideoPath) {
		err := utils.Copy(m.Video().Path(), sourceVideoPath)
		if err != nil {
			return "", err
		}
	}
	err := backupAudio(sourceVideoPath, targetAudioPath)
	if err != nil {
		return "", err
	}
	err = utils.Copy(targetAudioPath, archiveAudioPath)
	if err != nil {
		return "", err
	}
	return targetAudioPath, nil
}

func backupAudio(sourceVideoPath, targetAudioPath string) error {
	audio := &types.Audio{}
	audio.SetPath(sourceVideoPath)
	maxBitrate := 160000 + 12000*audio.Channels()
	tmpFile := filepath.Join(utils.MkTmpDir(), "audio.mka")
	ffmpegParams := []string{}
	ffmpegParams = append(ffmpegParams, "-i", sourceVideoPath)
	ffmpegParams = append(ffmpegParams, "-vn")
	// Add light compression if an inefficient codec is used.
	if audio.Bitrate() > maxBitrate || audio.IsEfficient() {
		ffmpegParams = append(ffmpegParams, "-map", "0:a")
		ffmpegParams = append(ffmpegParams, "-c:a", "libopus")
		ffmpegParams = append(ffmpegParams, "-ab", strconv.Itoa(maxBitrate))
		ffmpegParams = append(ffmpegParams, "-ac", strconv.Itoa(audio.Channels()))
	} else {
		ffmpegParams = append(ffmpegParams, "-acodec", "copy")
	}
	ffmpegParams = append(ffmpegParams, "-y")
	ffmpegParams = append(ffmpegParams, tmpFile)
	utils.PrintFfmpeg(ffmpegParams)
	err := exec.Command(utils.FfmpegPath(), ffmpegParams...).Run() // #nosec
	if err != nil {
		log.Println("Could not archive audio with libopus codec, trying aac codec.")
		ffmpegParams := []string{}
		ffmpegParams = append(ffmpegParams, "-i", sourceVideoPath)
		ffmpegParams = append(ffmpegParams, "-vn")
		ffmpegParams = append(ffmpegParams, "-map", "0:a")
		ffmpegParams = append(ffmpegParams, "-c:a", "aac")
		ffmpegParams = append(ffmpegParams, "-ab", strconv.Itoa(224000+12000*audio.Channels()))
		ffmpegParams = append(ffmpegParams, "-ac", strconv.Itoa(audio.Channels()))
		ffmpegParams = append(ffmpegParams, "-y")
		ffmpegParams = append(ffmpegParams, tmpFile)
		utils.PrintFfmpeg(ffmpegParams)
		err := exec.Command(utils.FfmpegPath(), ffmpegParams...).Run() // #nosec
		if err != nil {
			log.Println("Unable to execute:", utils.FfmpegPath(), strings.Join(ffmpegParams, " "))
			return err
		}
	}
	return os.Rename(tmpFile, targetAudioPath)
}
