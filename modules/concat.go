/*
Package modules containing the logic to concatinate videos together.
When videos are not the same resolution they will be letterboxed.
*/
package modules

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"gitlab.com/geo-gs/twombps/log"
	"gitlab.com/geo-gs/twombps/params"
	"gitlab.com/geo-gs/twombps/types"
	"gitlab.com/geo-gs/twombps/utils"
)

// Concat concatenates all supplied input videos
func Concat(inputs []string, title string) string {
	start := time.Now()
	log.Println("### Concatenating videos.")
	if params.GetParameters().DryRun() {
		return ""
	}
	tmpDir, _ := ioutil.TempDir(os.TempDir(), "optimize")
	tmpFile := filepath.Join(tmpDir, "concat.mkv")
	defer os.RemoveAll(tmpDir)
	videos := make([]*types.Video, 0)
	for _, input := range inputs {
		name := strings.TrimSuffix(path.Base(input), path.Ext(input))
		v := types.Video{}
		v.SetName(name)
		v.SetPath(input)
	}
	if !copyAll(videos, tmpDir) {
		log.Println("Could not copy all videos.")
		return ""
	}
	if !sanitizeAll(videos) {
		log.Println("Could not sanitize all videos.")
		return ""
	}
	if !joinAll(videos, title, tmpFile) {
		log.Println("Could not join all videos.")
		return ""
	}
	log.Println(title, "concatenation done in", time.Since(start).Hours(), "hours")
	return tmpFile
}

func findAll(path string, title string) []*types.Video {
	videos := make([]*types.Video, 0)
	for i := 0; i < 9000; i++ {
		files, err := ioutil.ReadDir(path + title)
		if err != nil {
			log.Println(err)
			continue
		}
		for _, file := range files {
			for _, extension := range params.VideoExtensions {
				suffix := fmt.Sprintf(" - pt%v.%s", i, extension)
				if strings.HasSuffix(file.Name(), suffix) {
					v := types.Video{}
					v.SetName(strings.TrimSuffix(file.Name(), suffix))
					v.SetPath(path + title + "/" + file.Name())
					videos = append(videos, &v)
				}
			}
		}
	}
	return videos
}

func copyAll(videos []*types.Video, toDir string) bool {
	for i, v := range videos {
		toFilePath := filepath.Join(toDir, strconv.Itoa(i+1000)+".mkv")
		err := utils.Copy(v.Path(), filepath.Join(toDir, strconv.Itoa(i+1000)+".mkv"))
		if err != nil {
			log.Println(err)
			return false
		}
		v.SetPath(toFilePath)
	}
	return true
}

func sanitizeAll(videos []*types.Video) bool {
	fps := videos[0].FrameRateStr()
	w, h, c, dar := max(videos)
	for _, v := range videos {
		if !sanitize(v, fps, w, h, c, dar) {
			return false
		}
	}
	return true
}

func joinAll(videos []*types.Video, title string, to string) bool {
	tmpFiles, _ := ioutil.TempDir(os.TempDir(), "join")
	defer os.RemoveAll(tmpFiles)
	utils.Write(filepath.Join(tmpFiles, "files.txt"), files(videos))
	utils.Write(filepath.Join(tmpFiles, "metadata.txt"), metadata(title, videos))
	params := []string{}
	params = append(params, "-f", "concat")
	params = append(params, "-safe", "0")
	params = append(params, "-i", filepath.Join(tmpFiles, "files.txt"))
	params = append(params, "-i", filepath.Join(tmpFiles, "metadata.txt"))
	params = append(params, "-map_metadata", "1")
	params = append(params, "-c", "copy")
	params = append(params, to)
	utils.PrintFfmpeg(params)
	err := exec.Command(utils.FfmpegPath(), params...).Run() // #nosec
	if err != nil {
		log.Println("Unable to execute: ffmpeg", strings.Join(params, " "))
		log.Println(err)
		return false
	}
	return true
}

func moveAll(videos []*types.Video, toDir string) bool {
	for _, v := range videos {
		err := os.Rename(v.Path(), toDir)
		if err != nil {
			log.Printf("Failed to move %v to %v: %v\n", v.Path(), toDir, err)
		}
	}
	return true
}

func sanitize(v *types.Video, fps string, w int, h int, c int, dar float64) bool {
	a := types.Audio{}
	a.SetPath(v.Path())
	vf := ""
	vf = vf + fmt.Sprintf("scale=(iw*sar)*min(%v/(iw*sar)\\,%v/ih):ih*min(%v/(iw*sar)\\,%v/ih):flags=print_info+spline+full_chroma_inp+full_chroma_int,", w, h, w, h)
	vf = vf + fmt.Sprintf("pad=%v:%v:(%v-iw*min(%v/iw\\,%v/ih))/2:(%v-ih*min(%v/iw\\,%v/ih))/2", w, h, w, w, h, w, w, h)
	err := os.Rename(v.Path(), v.Path()+".orig")
	if err != nil {
		log.Println(err)
		return false
	}
	ffmpegParams := []string{}
	ffmpegParams = append(ffmpegParams, "-i", v.Path()+".orig")
	ffmpegParams = append(ffmpegParams, "-map", "0:v:0")
	ffmpegParams = append(ffmpegParams, "-map", "-0:t") // remove attachments
	ffmpegParams = append(ffmpegParams, "-vf", vf)
	ffmpegParams = append(ffmpegParams, "-vsync", "1")
	ffmpegParams = append(ffmpegParams, "-vcodec", "libx264")
	ffmpegParams = append(ffmpegParams, "-r", fps)
	// crf is set to 6 when preset is slow.
	ffmpegParams = append(ffmpegParams, "-crf", strconv.Itoa(3+params.GetParameters().PresetInt()))
	ffmpegParams = append(ffmpegParams, "-preset", params.GetParameters().Preset())
	ffmpegParams = append(ffmpegParams, "-profile:v", "high10")
	ffmpegParams = append(ffmpegParams, "-level:v", "6.1")
	ffmpegParams = append(ffmpegParams, "-g", "60")
	ffmpegParams = append(ffmpegParams, "-sc_threshold", "0")
	ffmpegParams = append(ffmpegParams, "-map", "0:a:0")
	ffmpegParams = append(ffmpegParams, "-c:a", "aac")
	ffmpegParams = append(ffmpegParams, "-filter:a", "aresample=async=1:min_hard_comp=0.100000:first_pts=0")
	ffmpegParams = append(ffmpegParams, "-ab", "600k")
	ffmpegParams = append(ffmpegParams, "-ac", strconv.Itoa(c))
	ffmpegParams = append(ffmpegParams, "-ar", "48000")
	ffmpegParams = append(ffmpegParams, "-y")
	ffmpegParams = append(ffmpegParams, v.Path())
	utils.PrintFfmpeg(ffmpegParams)
	err = exec.Command(utils.FfmpegPath(), ffmpegParams...).Run() // #nosec
	if err != nil {
		log.Println("Unable to execute: ffmpeg", strings.Join(ffmpegParams, " "))
		log.Println(err)
		return false
	}
	return true
}

func max(videos []*types.Video) (int, int, int, float64) {
	w := 0
	h := 0
	c := 0
	d := float64(0)
	for _, v := range videos {
		a := types.Audio{}
		a.SetPath(v.Path())
		if a.Channels() > c {
			c = a.Channels()
		}
		if v.Width() > w {
			w = v.Width()
		}
		if v.Height() > h {
			h = v.Height()
		}
		if v.Dar64() > d {
			d = v.Dar64()
		}
	}
	// When the DAR is larger than 16:9 then the width fills the frame and the height should be proportional.
	// Otherwise the height fills the frame and the width should be proportional.
	if d > 1.777 {
		h = int(float64(w) / d)
	} else {
		w = int(float64(h) * d)
	}
	return w, h, c, d
}

func metadata(title string, videos []*types.Video) string {
	chapterData := ";FFMETADATA1\n"
	chapterData = chapterData + fmt.Sprintf("title=%s\n\n", title)
	start := 0
	end := 0
	for i, v := range videos {
		end = end + v.DurationMs()
		chapterData = chapterData + "[CHAPTER]\n"
		chapterData = chapterData + "TIMEBASE=1/1000\n"
		chapterData = chapterData + fmt.Sprintf("START=%v\n", start)
		chapterData = chapterData + fmt.Sprintf("END=%v\n", end)
		chapterData = chapterData + fmt.Sprintf("title=CHAPTER %v: %s\n\n", i+1, v.Name())
		start = end
	}
	// TrimSuffix will only trim the last match.
	// TrimRight will trim multiple all matches
	// return strings.TrimSuffix(chapterData, "\n")
	return strings.TrimRight(chapterData, "\n")
}

func files(videos []*types.Video) string {
	files := ""
	for _, v := range videos {
		files = files + fmt.Sprintf("file '%v'\n", v.Path())
	}
	return files
}
