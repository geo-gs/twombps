/*
Package codecs containing the logic to construct the HEVC parameters·
for the ffmpeg optimization.
*/
package codecs

import (
	"strconv"
	"strings"

	"gitlab.com/geo-gs/twombps/params"
)

// HevcParams returns the ffmpeg HEVC params to use to encode the video.
func HevcParams(frameRate float64) []string {
	ffmpegParams := []string{}
	ffmpegParams = append(ffmpegParams, "-preset", params.GetParameters().Preset())
	ffmpegParams = append(ffmpegParams, "-profile:v", params.GetParameters().VideoProfile())
	h265Params := []string{}
	// Set the video level to 4.0 as its a good balance between compatibility and quality.
	h265Params = append(h265Params, "level-idc=40")
	// Set the max frames between keyframes.
	h265Params = append(h265Params, "keyint="+strconv.Itoa(int(frameRate)*10))
	// These values need to be supplied to the x265 codec directly.
	ffmpegParams = append(ffmpegParams, "-x265-params", strings.Join(h265Params, ":"))
	return ffmpegParams
}
