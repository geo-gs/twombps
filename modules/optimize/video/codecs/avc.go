/*
Package codecs containing the logic to construct the AVC parameters·
for the ffmpeg optimization.
*/
package codecs

import (
	"strconv"

	"gitlab.com/geo-gs/twombps/params"
)

// AvcParams returns the ffmpeg AVC params to use to encode the video.
func AvcParams(frameRate float64) []string {
	ffmpegParams := []string{}
	ffmpegParams = append(ffmpegParams, "-preset", params.GetParameters().Preset())
	ffmpegParams = append(ffmpegParams, "-profile:v", params.GetParameters().VideoProfile())
	// Set the video level to 4.0 as its a good balance between compatibility and quality.
	ffmpegParams = append(ffmpegParams, "-level:v", "4.0")
	// Set the max frames between keyframes.
	ffmpegParams = append(ffmpegParams, "-g", strconv.Itoa(int(frameRate)*10))
	return ffmpegParams
}
