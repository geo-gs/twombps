/*
Package codecs containing the logic to construct the AV1 parameters
for the ffmpeg optimization.
*/
package codecs

import (
	"strconv"

	"gitlab.com/geo-gs/twombps/params"
)

// Av1Params returns the ffmpeg AV1 params to use to encode the video.
func Av1Params(frameRate float64, maxBitrate int) []string {
	ffmpegParams := []string{}
	// https://brontosaurusrex.github.io/2021/06/05/AV1-encoding-for-dummies/
	// Setting the denoise-noise-level parameter enables grain synthesis.
	ffmpegParams = append(ffmpegParams, "-denoise-noise-level", "15")
	// The cpu-used parameter sets how efficient the compression will be. Default is 1. Lower values mean slower
	// encoding with better quality, and vice-versa. Valid values are from 0 to 8 inclusive.
	ffmpegParams = append(ffmpegParams, "-cpu-used", strconv.Itoa(params.GetParameters().PresetInt()))
	// arnr settings used according to https://www.reddit.com/r/AV1/comments/t59j32/encoder_tuning_part_4_a_2nd_generation_guide_to/
	// fixes keyfame blocking
	ffmpegParams = append(ffmpegParams, "-arnr-max-frames", "3")
	ffmpegParams = append(ffmpegParams, "-arnr-strength", "1")
	// aq-mode: https://www.reddit.com/r/AV1/comments/t59j32/encoder_tuning_part_4_a_2nd_generation_guide_to/
	ffmpegParams = append(ffmpegParams, "-aq-mode", "1")
	// Setting the lag-in-frames to 48 lets the AV1 codec look ahead 48 frames into the future.
	// Higher values improve visual quality.
	// 33 and higher: Scene detection mode 2 is active due to large number of future references
	// allowing for the highest level of scene detection present in aomenc and more information is gathered
	ffmpegParams = append(ffmpegParams, "-lag-in-frames", "48")
	// Enable use of alternate reference frames.
	ffmpegParams = append(ffmpegParams, "-auto-alt-ref", "1")
	ffmpegParams = append(ffmpegParams, "-aom-params", "enable-qm=1")
	// Set the max frames between keyframes
	ffmpegParams = append(ffmpegParams, "-g", strconv.Itoa(int(frameRate)*10))
	// AV1 must define an average bitrate parameter to be able to implement rate control parameters (ie: capped CRF encoding).
	// Set average bitrate to be 250kbps less than the max bitrate.
	ffmpegParams = append(ffmpegParams, "-b:v", strconv.Itoa(maxBitrate-250000))
	return ffmpegParams
}
