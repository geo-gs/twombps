/*
Package filters containing the logic to build the video filter for
the ffmpeg optimization when no resizing should occur.
*/
package filters

import (
	"fmt"
	"strings"

	"gitlab.com/geo-gs/twombps/params"
	"gitlab.com/geo-gs/twombps/types"
)

// VfNoResize returns the ffmpeg video filters to apply to the video.
func VfNoResize(srcWidth, srcHeight int, crop *types.Crop, frameRateStr, srcPixFmt string, progressive bool) string {
	vf := []string{}
	// If decombing is enabled and the input video has more interlaced frames than progressive frames then lets deinterlace it first.
	// Note: Handbrake's decomb option provides a better result. Use that when possible; this is just here as a fail-safe.
	if !params.GetParameters().Ultrafast() && params.GetParameters().Decomb() && !progressive {
		// https://macilatthefront.blogspot.com/2021/05/which-deinterlacing-algorithm-is-best.html
		vf = append(vf, "bwdif")
	}
	// Crop Video.
	// No need to waist resolution here when PLEX lets us use anamorphic scaling in 720p.
	// Only crop when the input reslution and the crop resolution are dfferent
	if crop.Width() != srcWidth && crop.Height() != srcHeight {
		vf = append(vf, crop.Filter())
	}
	// Configure denoising; pixel format should end up in yuv420p10le.
	if params.GetParameters().PresetGroup() == 0 || !params.GetParameters().Denoise() {
		if srcPixFmt != "yuv420p10le" {
			vf = append(vf, "format=yuv420p10le")
		}
	} else {
		// nlmeans needs the video format to be in yuv420p or it crashes.
		if srcPixFmt != "yuv420p" {
			vf = append(vf, "format=yuv420p")
		}
		vf = append(vf, "nlmeans='1.0:7:5:3:3'", "format=yuv420p10le")
	}
	// Convert back to 8bit only when forced to.
	if params.GetParameters().Force8Bit() {
		vf = append(vf, "format=yuv420p")
	}
	vf = append(vf, fmt.Sprintf("fps='fps=%s'", frameRateStr))
	return strings.Join(vf, ",")
}
