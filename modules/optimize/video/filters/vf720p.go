/*
Package filters containing the logic to build the 720p video filter
for the ffmpeg optimization.
*/
package filters

import (
	"fmt"
	"strings"

	"gitlab.com/geo-gs/twombps/params"
	"gitlab.com/geo-gs/twombps/types"
	"gitlab.com/geo-gs/twombps/utils"
)

// Vf720p returns the ffmpeg 720p video filters to apply to the video.
func Vf720p(srcWidth, srcHeight int, crop *types.Crop, frameRateStr, dar, srcPixFmt, srcColorPrimaries string, progressive bool) string {
	vf := []string{}
	// If decombing is enabled and the input video has more interlaced frames than progressive frames then lets deinterlace it first.
	// Note: Handbrake's decomb option provides a better result. Use that when possible; this is just here as a fail-safe.
	if !params.GetParameters().Ultrafast() && params.GetParameters().Decomb() && !progressive {
		// https://macilatthefront.blogspot.com/2021/05/which-deinterlacing-algorithm-is-best.html
		vf = append(vf, "bwdif")
	}
	// Crop Video.
	// No need to waist resolution here when PLEX lets us use anamorphic scaling in 720p.
	// Only crop when the input reslution and the crop resolution are dfferent
	if crop.Width() != srcWidth && crop.Height() != srcHeight {
		vf = append(vf, crop.Filter())
	}
	// If cropped video resolution is close to exactly half the height of 720p then lets crop out the
	//   middle 360 vertical pixels so that we don't end up bluring the vertical resolution to accomidate
	//   a couple of edge pixels that don't really matter.
	if crop.Height() < 376 && crop.Height() > 360 {
		vf = append(vf, fmt.Sprintf("crop=%d:360,", crop.Width()))
	}
	// Migrate Video to 720p colorspace. Not doing so will cause playback issues on some players.
	targetColorPrimaries := "bt709"
	if srcColorPrimaries == "unknown" {
		if srcHeight > 720 {
			vf = append(vf, fmt.Sprintf("colorspace=%s:iall=%s:fast=1", targetColorPrimaries, "bt2020"))
		} else if srcHeight > 480 {
			vf = append(vf, fmt.Sprintf("colorspace=%s:iall=%s:fast=1", targetColorPrimaries, "bt709"))
		} else if frameRateStr == "25/1" || frameRateStr == "50/1" {
			vf = append(vf, fmt.Sprintf("colorspace=%s:iall=%s:fast=1", targetColorPrimaries, "bt601-6-625"))
		} else {
			vf = append(vf, fmt.Sprintf("colorspace=%s:iall=%s:fast=1", targetColorPrimaries, "bt601-6-525"))
		}
	} else if srcColorPrimaries != "bt709" {
		vf = append(vf, fmt.Sprintf("colorspace=%s:iall=%s:fast=1", targetColorPrimaries, srcColorPrimaries))
	}
	// Configure denoising; pixel format should end up in yuv420p10le.
	if params.GetParameters().PresetGroup() == 0 || !params.GetParameters().Denoise() {
		if srcPixFmt != "yuv420p10le" {
			vf = append(vf, "format=yuv420p10le")
		}
	} else {
		// nlmeans needs the video format to be in yuv420p or it crashes.
		if srcPixFmt != "yuv420p" {
			vf = append(vf, "format=yuv420p")
		}
		vf = append(vf, "nlmeans='1.0:7:5:3:3'", "format=yuv420p10le")
	}
	// Only use nural network AI super-resolution when preset is not: ultrafast, superfast, veryfast, faster
	if params.GetParameters().PresetGroup() != 0 && params.GetParameters().Nnedi() {
		nnediFilter := fmt.Sprintf("nnedi=weights='%s':nsize='s16x6':nns='n64':pscrn='new':field='af'", utils.NnediWeightsPath())
		if crop.Width() < 1000 && crop.Height() < 600 {
			vf = append(vf, "scale=w=iw*2:h=ih*2:flags=print_info+spline+full_chroma_inp+full_chroma_int")
			vf = append(vf, nnediFilter)
			vf = append(vf, "transpose=1")
			vf = append(vf, nnediFilter)
			vf = append(vf, "transpose=2")
		} else if crop.Width() < 1000 {
			vf = append(vf, "scale=w=iw*2:h=ih:flags=print_info+spline+full_chroma_inp+full_chroma_int")
			vf = append(vf, "transpose=1")
			vf = append(vf, nnediFilter)
			vf = append(vf, "transpose=2")
		} else if crop.Height() < 600 {
			vf = append(vf, "scale=w=iw:h=ih*2:flags=print_info+spline+full_chroma_inp+full_chroma_int")
			vf = append(vf, nnediFilter)
		}
	}
	// Force the output video to be 720p.
	if crop.Width() != 1280 || crop.Height() != 720 {
		vf = append(vf, fmt.Sprintf("scale=w=1280:h=720:out_color_matrix=%s:flags=print_info+%s+full_chroma_inp+full_chroma_int", targetColorPrimaries, params.GetParameters().ScalingAlgo()))
		// Run a light denoiser and light sharpener to clean up any jitters created by scaling.
		if !params.GetParameters().Ultrafast() {
			vf = append(vf, "hqdn3d=1:1:9:9")
			vf = append(vf, "unsharp=5:5:0.8:3:3:0.4")
		}
	}
	// Convert back to 8bit only when forced to.
	if params.GetParameters().Force8Bit() {
		vf = append(vf, "format=yuv420p")
	}
	vf = append(vf, fmt.Sprintf("fps='fps=%s'", frameRateStr))
	return strings.Join(vf, ",")
}
